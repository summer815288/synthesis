package svc

import (
	"github.com/zeromicro/go-zero/zrpc"
	"synthesis/app/admin/internal/config"
	"synthesis/app/micro/ads/microads"
)

type ServiceContext struct {
	Config       config.Config
	AdsRpcClient microads.MicroAds
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:       c,
		AdsRpcClient: microads.NewMicroAds(zrpc.MustNewClient(c.AdsRpcConf)),
	}
}
