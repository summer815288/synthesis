package logic

import (
	"context"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"synthesis/app/micro/ads/microads"

	"synthesis/app/admin/adminpb"
	"synthesis/app/admin/internal/svc"

	"github.com/zeromicro/go-zero/core/logx"
)

type AdsListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAdsListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AdsListLogic {
	return &AdsListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AdsListLogic) AdsList(in *adminpb.AdminAdsListReq) (*adminpb.AdminAdsListRes, error) {
	// todo: add your logic here and delete this line
	adsRrsp, err := l.svcCtx.AdsRpcClient.AdsList(l.ctx, &microads.MicroAdsListReq{
		OrderField: in.OrderField,
		OrderSort:  in.OrderSort,
		Page:       in.Page,
		Limit:      in.Limit,
		Status:     in.Status,
		History:    in.History})
	if err != nil {
		return nil, errors.Wrapf(err, "req: %+v", in)
	}
	var resp adminpb.AdminAdsListRes
	_ = copier.Copy(&resp, adsRrsp)

	return &resp, nil
}
