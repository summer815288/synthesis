package logic

import (
	"context"
	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"synthesis/common/xerr"
	"time"

	"synthesis/app/micro/ads/adspb"
	"synthesis/app/micro/ads/internal/svc"

	"github.com/zeromicro/go-zero/core/logx"
)

type AdsListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAdsListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AdsListLogic {
	return &AdsListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AdsListLogic) AdsList(in *adspb.MicroAdsListReq) (*adspb.MicroAdsListRes, error) {
	// todo: add your logic here and delete this line
	//接收值
	orderFields := []string{"id", "create_time", "start_time", "end_time"}
	OrderField := IsContain(orderFields, in.OrderField, "create_time")
	if in.Page < 0 {
		in.Page = 1
	}
	if in.Limit < 0 {
		in.Limit = 10
	}
	//******************************************去数据库中查到数据、循环处理、输出******************************************
	//1、判断history
	timeUnix := time.Now().Unix()
	endTimeSql := " ( `end_time` between 1 and  ? ) "
	if in.History == true {
		endTimeSql = " ( `end_time` = 0 OR `end_time` >  ? ) "
	}
	whereBuilder := l.svcCtx.WmAdsModel.RowBuilder().Where(endTimeSql, timeUnix)
	countBuilder := l.svcCtx.WmAdsModel.CountBuilder("*").Where(endTimeSql, timeUnix)
	//2、判断status
	if in.Status != 0 {
		whereBuilder = whereBuilder.Where(squirrel.Eq{
			"status": in.Status,
		})
		countBuilder = countBuilder.Where(squirrel.Eq{
			"status": in.Status,
		})
	}
	//3、获取count值
	count, err := l.svcCtx.WmAdsModel.FindCount(l.ctx, countBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError), "get adsModel count err : %v", err)
	}
	//4、获取数据
	var resp []*adspb.AdsCommon_Item
	if count > 0 {
		list, err := l.svcCtx.WmAdsModel.FindPageListByPage(l.ctx, whereBuilder, in.Page, in.Limit, OrderField+" "+in.OrderSort)
		if err != nil {
			return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError), "get adsModel list err : %v", err)
		}
		if len(list) > 0 {
			for _, item := range list {
				var adsItem adspb.AdsCommon_Item
				_ = copier.Copy(&adsItem, item)
				//item.CreateTime = item.CreateTime.Unix()
				resp = append(resp, &adsItem)
			}
		}
	}
	//var resp []types.AdsItem
	//if len(list) > 0 { // mapreduce example
	//	mr.MapReduceVoid(func(source chan<- interface{}) {
	//		for _, v := range list {
	//			source <- v.Id
	//		}
	//	}, func(item interface{}, writer mr.Writer, cancel func(error)) {
	//		id := item.(int64)
	//
	//		adsItem, err := l.svcCtx.WmAdsModel.FindOne(l.ctx, id)
	//		if err != nil && err != model.ErrNotFound {
	//			logx.WithContext(l.ctx).Errorf("ActivityHomestayListLogic ActivityHomestayList 获取活动数据失败 id : %d ,err : %v", id, err)
	//			return
	//		}
	//		writer.Write(adsItem)
	//	}, func(pipe <-chan interface{}, cancel func(error)) {
	//
	//		for item := range pipe {
	//			homestay := item.(*model.WmAds)
	//			var tyHomestay types.AdsItem
	//			_ = copier.Copy(&tyHomestay, homestay)
	//			resp = append(resp, tyHomestay)
	//		}
	//	})
	//}

	return &adspb.MicroAdsListRes{
		List:  resp,
		Count: count,
	}, nil
}

/**
判断值在不在
*/
func IsContain(items []string, item string, defaultValue string) string {
	for _, eachItem := range items {
		if eachItem == item {
			return item
		}
	}
	return defaultValue
}
