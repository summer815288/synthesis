package svc

import (
	"github.com/zeromicro/go-zero/core/stores/sqlx"
	"synthesis/app/micro/ads/internal/config"
	"synthesis/models/adsModel"
)

type ServiceContext struct {
	Config config.Config
	WmAdsModel adsModel.WmAdsModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config: c,
		WmAdsModel: adsModel.NewWmAdsModel(sqlx.NewMysql(c.DB.DataSource), c.Cache),
	}
}
