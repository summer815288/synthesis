// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.19.4
// source: protos/micro/ads.proto

package adspb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	MicroAds_AdsList_FullMethodName = "/micro.MicroAds/AdsList"
)

// MicroAdsClient is the client API for MicroAds service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MicroAdsClient interface {
	AdsList(ctx context.Context, in *MicroAdsListReq, opts ...grpc.CallOption) (*MicroAdsListRes, error)
}

type microAdsClient struct {
	cc grpc.ClientConnInterface
}

func NewMicroAdsClient(cc grpc.ClientConnInterface) MicroAdsClient {
	return &microAdsClient{cc}
}

func (c *microAdsClient) AdsList(ctx context.Context, in *MicroAdsListReq, opts ...grpc.CallOption) (*MicroAdsListRes, error) {
	out := new(MicroAdsListRes)
	err := c.cc.Invoke(ctx, MicroAds_AdsList_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MicroAdsServer is the server API for MicroAds service.
// All implementations must embed UnimplementedMicroAdsServer
// for forward compatibility
type MicroAdsServer interface {
	AdsList(context.Context, *MicroAdsListReq) (*MicroAdsListRes, error)
	mustEmbedUnimplementedMicroAdsServer()
}

// UnimplementedMicroAdsServer must be embedded to have forward compatible implementations.
type UnimplementedMicroAdsServer struct {
}

func (UnimplementedMicroAdsServer) AdsList(context.Context, *MicroAdsListReq) (*MicroAdsListRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AdsList not implemented")
}
func (UnimplementedMicroAdsServer) mustEmbedUnimplementedMicroAdsServer() {}

// UnsafeMicroAdsServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MicroAdsServer will
// result in compilation errors.
type UnsafeMicroAdsServer interface {
	mustEmbedUnimplementedMicroAdsServer()
}

func RegisterMicroAdsServer(s grpc.ServiceRegistrar, srv MicroAdsServer) {
	s.RegisterService(&MicroAds_ServiceDesc, srv)
}

func _MicroAds_AdsList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MicroAdsListReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MicroAdsServer).AdsList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MicroAds_AdsList_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MicroAdsServer).AdsList(ctx, req.(*MicroAdsListReq))
	}
	return interceptor(ctx, in, info, handler)
}

// MicroAds_ServiceDesc is the grpc.ServiceDesc for MicroAds service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var MicroAds_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "micro.MicroAds",
	HandlerType: (*MicroAdsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AdsList",
			Handler:    _MicroAds_AdsList_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "protos/micro/ads.proto",
}
