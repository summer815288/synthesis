rpc服务代码生成-以admin为例：admin、external、internal rpc代码的生成是一样的

# 一、项目初始化

1、进入synthesis项目根目录

2、生成admin-rpc

```shell
protoc  protos/admin/message/adscommon.proto --go_out=app/admin/ --go-grpc_out=app/admin/

goctl rpc protoc  protos/admin/admin.proto --go_out=app/admin/ --go-grpc_out=app/admin/ --zrpc_out=app/admin/  -style goZero

```

3、生成internal-rpc

```shell
protoc  protos/internal/message/adscommon.proto --go_out=app/internal/ --go-grpc_out=app/internal/

goctl rpc protoc  protos/internal/internal.proto --go_out=app/internal/ --go-grpc_out=app/internal/ --zrpc_out=app/internal/  -style goZero

```

4、生成external-rpc

```shell
protoc  protos/external/message/adscommon.proto --go_out=app/external/ --go-grpc_out=app/external/

goctl rpc protoc  protos/external/external.proto --go_out=app/external/ --go-grpc_out=app/external/ --zrpc_out=app/external/  -style goZero

```

5、生成micro-rpc

```shell
protoc  protos/micro/message/adscommon.proto --go_out=app/micro/ads/ --go-grpc_out=app/micro/ads/

goctl rpc protoc  protos/micro/ads.proto --go_out=app/micro/ads/ --go-grpc_out=app/micro/ads/ --zrpc_out=app/micro/ads/  -style goZero

```

6、编写代码：admin/internal/external 调用 micro中的rpc服务；

7、启动项目: 本地使用modd热启动

# 二、部署项目

1、在rpc服务里生成dockerFile

```shell

cd app/admin  ; goctl docker -go admin.go

cd ../internal  ; goctl docker -go internal.go

cd ../external  ; goctl docker -go external.go

cd ../micro/ads  ; goctl docker -go ads.go

```

2、回到项目根目录synthesis，执行编译

```shell
cd ....;
docker build -t synthesis_admin:v1 -f app/admin/Dockerfile . ;
docker build -t synthesis_micro_ads:v1 -f app/micro/ads/Dockerfile .;

```

***********************************************************************
ps：本地环境：执行docker-composer-env.yml
```shell
docker-compose -f docker-compose-env.yml up -d
```
***********************************************************************

3、执行docker-compose运行启动(如果是本地环境还需要执行)

```shell
docker-compose up -d
```

4、删除dockerfile文件

```shell
rm -rf app/admin/Dockerfile;
rm -rf app/internal/Dockerfile;
rm -rf app/external/Dockerfile;
rm -rf app/micro/ads/Dockerfile;

```